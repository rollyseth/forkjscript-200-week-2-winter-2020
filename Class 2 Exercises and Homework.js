// 1. Create an object representation of yourself
// Should include: 
// - firstName
// - lastName
// - 'favorite food'
// - bestFriend (object with the same 3 properties as above)


console.log(" ****** Task 1 ***** ");
const person = {    
  firstName: 'Rolly',
  lastName:'Seth',
  'favorite food':'Dumpling',
    bestFriend: 
      { firstName: 'Julie',
        lastName: 'Saathoff',
        'favorite food': 'Starbucks',
       }
  }; 



// 2. console.log best friend's firstName and your favorite food
console.log(`My best friend\'s first name is \'${person.bestFriend.firstName}\' and her favorite food is \'${person.bestFriend["favorite food"]}\' `);

// 3. Create an array to represent this tic-tac-toe board
// -O-
// -XO
// X-X

console.log(" ****** Task 2 ***** ");

const ticTacToeArray =
[ ['-','0','-'],
  ['-','X','0'],
  ['X','-','X'],              
];
console.log(`Original Array is: \n ${ticTacToeArray[0]} \n ${ticTacToeArray[1]} \n ${ticTacToeArray[2]}`);



// 4. After the array is created, 'O' claims the top right square.
// Update that value.
ticTacToeArray[0][2]='0';

// 5. Log the grid to the console.
console.log(`Updated Array is: \n ${ticTacToeArray[0]} \n ${ticTacToeArray[1]} \n ${ticTacToeArray[2]}`);



// 6. You are given an email as string myEmail, make sure it is in correct email format.
// Should be 1 or more characters, then @ sign, then 1 or more characters, then dot, then one or more characters - no whitespace
// i.e. foo@bar.baz
// Hints:
// - Use rubular to check a few emails: https://rubular.com/
// - Use regexp test method https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp/test
console.log(" ****** Task 3 ***** ");
const myEmail='rseth@uw.edu';
const regex=/\w+[@]\w+[\.]\w+/g;
console.log(`Is ${myEmail} an email?: ${regex.test(myEmail)}`); 

// 7. You are given an assignmentDate as a string in the format "month/day/year"
// i.e. '1/21/2019' - but this could be any date.
// Convert this string to a Date
const assignmentDate = '1/21/2019';


console.log(" ****** Converting String To Date ***** ");

var strDate = new Date(assignmentDate);
console.log (`Date equivalent of ${assignmentDate} is ${strDate}`);

// 8. Create a new Date instance to represent the dueDate.  
// This will be exactly 7 days after the assignment date.

var Date2= new Date(assignmentDate);
var dueDate= new Date(Date2.setDate(Date2.getDate()+7)); 
console.log (`Date 7 days from ${strDate} is ${dueDate}`);

// 9. Use dueDate values to create an HTML time tag in format
// <time datetime="YYYY-MM-DD">Month day, year</time>
// I have provided a months array to help


const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
];


const dueYear= dueDate.getFullYear();
const dueMonth=dueDate.getMonth();
const dueDay=dueDate.getDate(); 
const dueMonthString= months[dueMonth];

const dueTimeString =`<time datetime='${dueYear}-${dueMonth+1}-${dueDay}'> ${dueMonthString} ${dueDay}, ${dueYear}</time>`; 


const d= new Date(); 
console.log(document.getElementById("pagedate"));
//const timeString="<time datetime='2020-01-21'>January 21, 2020</time>"
document.getElementById("pagedate").innerHTML=dueTimeString; 

// 10. log this value using console.log

 // document.write("Due Date is:"+ months[newDay]);
console.log(`Due Date Time Tag:`);